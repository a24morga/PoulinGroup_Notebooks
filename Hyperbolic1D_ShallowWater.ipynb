{
 "cells": [
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Expand the view to scale with window width\n",
    "from IPython.core.display import display, HTML\n",
    "display(HTML(\"<style>.container { width:90% !important; }</style>\"))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<h1 align=\"center\">\n",
    "ELMS1\n",
    "</h1>\n",
    "<h2 align=\"center\">\n",
    "One Dimensional Wave Equation\n",
    "</h2>\n",
    "<h3 align=\"center\">\n",
    "By Francis J. Poulin, Adam Morgan, and Ben Storer\n",
    "</h3>\n",
    "<h4 align=\"center\">\n",
    "Last modified: 31 January, 2018\n",
    "</h4>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# One Dimensional Wave Equation #\n",
    "\n",
    "## Model Equations ##\n",
    "The one-dimensional (1D) shallow water model with variable depth $H(x)$ may be written as a system of two coupled first-order PDEs,\n",
    "\\begin{align*}\n",
    "\\partial_t u & = - g \\partial_x h - \\beta u, \\ \\text{and} \\\\\n",
    "\\partial_t h & = - H(x) \\ \\partial_x u.\n",
    "\\end{align*}\n",
    "Note that $g$ is the acceleration due to gravity, $H(x)$ is the mean depth of the fluid, $h(x,t)$ is the deformation of the surface, and $u(x,t)$ is the velocity.\n",
    "\n",
    "The boundary conditions that we impose is that the velocity is zero at the walls, so\n",
    "$$\n",
    "u(0, t) = 0,\n",
    "\\quad \\mbox{ and } \\quad\n",
    "u(L_x, t) = 0.\n",
    "$$\n",
    "\n",
    "We must also impose initial conditions on both fields:\n",
    "$$\n",
    "u(x,0) = f(x),\n",
    "\\quad \\mbox{ and } \\quad\n",
    "h(x,0) = g(x).\n",
    "$$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Weak Form ##\n",
    "\n",
    "We assume the solution is known at $t^n$ and therefore the unknown functions to be determined are $u^{n+1}, h^{n+1}$.  We define the trial functions $u^*, h^*$ and the test functions $v, j$, then multiply each equation by its respective test function to obtain,\n",
    "\n",
    "\\begin{align*}\n",
    "\\int v u^*  \n",
    "+  \\frac{\\Delta t}{2} \\left( \\beta u^* + g v \\partial_x h^* \\right) \n",
    " \\, dx \n",
    "& = \\int v u^n \n",
    "- \\frac{\\Delta t}{2} \\left( \\beta u^n + g v \\partial_x h^n \\right) \\, dx, \\\\\n",
    "\\int j h^*  + \\frac{\\Delta t}{2}  H(x) \\ j \\ \\partial_x u^* \\, dx \n",
    "& = \\int j h^n - \\frac{\\Delta t}{2} H(x) \\ j \\ \\partial_x u^n \\, dx.\n",
    "\\end{align*}\n",
    "\n",
    "We integrate by parts in the third term on each side of the first equation to get\n",
    "\\begin{align*}\n",
    "\\int v u^*  \n",
    "+  \\frac{\\Delta t}{2} \\left( \\beta u^* - g h^* \\partial_x v \\right) \\, dx \n",
    "& = \\int v u^n \n",
    "- \\frac{\\Delta t}{2} \\left( \\beta u^n - g h^n \\partial_xv   \\right) \\, dx, \\\\\n",
    "\\int j h^*  + \\frac{\\Delta t}{2}  H(x) \\ j \\ \\partial_x u^* \\, dx \n",
    "& = \\int j h^n\n",
    "- \\frac{\\Delta t}{2} H(x) \\ j \\ \\partial_x u^n \\, dx.\n",
    "\\end{align*}\n",
    "\n",
    "Next, we add the equations to get\n",
    "\\begin{align*}\n",
    "\\int v u^* + j h^* +\n",
    "\\frac{\\Delta t}{2} \\left( \\beta u^* - g h^* \\partial_x v + H(x) \\ j \\ \\partial_x u^* \\right) \\, dx \n",
    "& = \\int v u^n + j h^n - \\frac{\\Delta t}{2} \\left( \\beta u^n - g h^n \\partial_x v + H(x) \\ j \\ \\partial_x u^n \\right) \\, dx.\n",
    "\\end{align*}\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Numerical Solution#\n",
    "\n",
    "Below we go through each step of the numerical method to solve the above weak form.\n",
    "\n",
    "#### Import Libraries"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import firedrake as fdr\n",
    "import numpy as np\n",
    "import matplotlib.pyplot as plt\n",
    "import matplotlib.animation as animation\n",
    "import cmocean\n",
    "from IPython.display import HTML\n",
    "import sys"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Specify parameters"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "T = 12.5         # Final time when the simulation stops\n",
    "\n",
    "Lx = 6.0         # Length of domain\n",
    "Nx = 200         # Number of elements\n",
    "\n",
    "g    = 1.0       # Gravity\n",
    "beta = 0.0       # Damping\n",
    "\n",
    "deg  = 4         # Degree of continuous approximating functions\n",
    "degD = deg-1     # Degree of discontinuous approximating functions\n",
    "                 #     Do not change this one"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Create the grid\n",
    "\n",
    "Since we have walls at the two ends, we impose no normal flow boudnary conditions i.e. $u=0$ at the two ends."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Method with homogeneous Dirichlet conditions\n",
    "mesh = fdr.IntervalMesh(Nx, Lx)\n",
    "\n",
    "# Function Spaces and grid\n",
    "V1 = fdr.FunctionSpace(mesh, \"CG\", deg)         \n",
    "V2 = fdr.FunctionSpace(mesh, \"DG\", degD)        \n",
    "W  = fdr.MixedFunctionSpace([V1,V2])            # Making a combined function space by taking the disjoint direct sum\n",
    "\n",
    "# Translate grid as a vector\n",
    "xdata = fdr.interpolate(fdr.Expression(\"x[0]\"),V2).vector().array()[1:]\n",
    "xplot = np.linspace(0, Lx, Nx)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Set the boundary conditions"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Dirichlet Boundary Conditions\n",
    "bcLeft  = fdr.DirichletBC(W.sub(1), fdr.Constant(0.0), 1)\n",
    "bcRight = fdr.DirichletBC(W.sub(1), fdr.Constant(0.0), 2)\n",
    "\n",
    "bcs = [bcLeft, bcRight]\n",
    "# Note: (\"1\" for left and \"2\" for right)    "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Wave Speed\n",
    "Under the shallow water equations, the wave speed is defined as\n",
    "\\begin{equation}\n",
    "c^2 = g H(x)\n",
    "\\end{equation}"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Define Depth\n",
    "H = fdr.Function(V1)\n",
    "H.interpolate(fdr.Expression(\"1.0+x[0]/2\", Lx=Lx));\n",
    "\n",
    "# Define wave speed\n",
    "c = fdr.interpolate(fdr.sqrt(g*H),V1)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Plot fluid depth\n",
    "plt.figure()\n",
    "plt.clf()\n",
    "fdr.plot(H)\n",
    "plt.title('Fluid Depth')\n",
    "plt.xlim([0, Lx])\n",
    "plt.xlabel('space')\n",
    "plt.grid('on')\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Plot wave speed\n",
    "plt.figure()\n",
    "plt.clf()\n",
    "fdr.plot(c)\n",
    "plt.title('Wave Speed')\n",
    "plt.xlim([0, Lx])\n",
    "plt.xlabel('space')\n",
    "plt.grid('on')\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Compute time step and temporal grid"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Mean wave speed\n",
    "cmean = np.absolute((1./Lx)*fdr.assemble(g*H*fdr.dx))\n",
    "\n",
    "# Time step and grid\n",
    "dt = 0.9*(Lx/(cmean*Nx)) #Time step, prescribed by Courant-Friedrichs-Lewy condition from numerical analysis\n",
    "Dt = fdr.Constant(dt)\n",
    "Nt = np.int(T/dt)\n",
    "tt = np.linspace(0., T, Nt)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Initial Conditions"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Define Solutions: w = [u,v]\n",
    "w0     = fdr.Function(W)    # Known solution\n",
    "w1     = fdr.Function(W)    # Unknown solution\n",
    "\n",
    "u0, h0 = w0.split()         # Split into two variables\n",
    "\n",
    "# Define initial conditions on the system\n",
    "h0.interpolate(fdr.Expression(\"exp(-9*pow(x[0]-Lx/2.,2))\", Lx=Lx));\n",
    "u0.interpolate(fdr.Expression(\"0.0\"));"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "\\begin{align*}\n",
    "\\int v u^* + j h^* +\n",
    "\\frac{\\Delta t}{2} \\left( \\beta u^* - g h^* \\partial_x v + H(x) j \\partial_x u^* \\right) \\, dx \n",
    "& = \\int v u^n + j h^n - \\frac{\\Delta t}{2} \\left( \\beta u^n - g h^n \\partial_x v + H(x) j \\partial_x u^n \\right) \\, dx.\n",
    "\\end{align*}"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def Step_Wave(w0, g, H, beta, bcs):\n",
    "    \n",
    "    # Define test and trial functions\n",
    "    v, j = fdr.TestFunctions(W)\n",
    "    u, h = fdr.TrialFunctions(W)\n",
    "\n",
    "    # Define weak form\n",
    "    # \"a\" is a bilinear form: a(u,v)\n",
    "    # \"L\" is a linear form:   L(v)\n",
    "    a = (v*u  + j*h  + 0.5*Dt*(beta*u - g*h*v.dx(0)  + H*j*u.dx(0)) )*fdr.dx\n",
    "    L = (v*u0 + j*h0 - 0.5*Dt*(beta*u - g*h0*v.dx(0) + H*j*u0.dx(0)))*fdr.dx    \n",
    "    \n",
    "    # Solve weak form        \n",
    "    fdr.solve(a == L, w1, bcs=bcs, solver_parameters={'mat_type': 'aij','ksp_type': 'preonly','pc_type': 'lu'})\n",
    "\n",
    "    return w1"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Create arrays to store the solution"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "hsoln = np.zeros((Nt,Nx))\n",
    "hsoln[0,:] = h0.at(xplot, tolerance=1e-12)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Loop over the time-steps, storing h as we go"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "for cnt in range(1,Nt):\n",
    "    t = cnt*dt\n",
    "\n",
    "    w1 = Step_Wave(w0, g, H, beta, bcs)\n",
    "    \n",
    "    w0.assign(w1)\n",
    "    \n",
    "    hsoln[cnt,:] = h0.at(xplot, tolerance=1e-12)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Hovmoeller Plot of h"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "plt.figure()\n",
    "cv = np.max(np.abs(hsoln))\n",
    "\n",
    "#plt.contourf(xplot,tt,hsoln, 20, cmap = cmocean.cm.balance, vmin=-cv, vmax=cv)\n",
    "plt.pcolormesh(xplot,tt,hsoln, cmap = cmocean.cm.balance, vmin=-cv, vmax=cv)\n",
    "\n",
    "plt.colorbar()\n",
    "plt.xlabel(\"x\")\n",
    "plt.ylabel(\"t\")\n",
    "plt.title(\"Evolution of Height Field h\")\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Make Animation"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "fig = plt.figure()\n",
    "ax = plt.axes(xlim=(0, 6.0), ylim=(-1, 1))\n",
    "ax.grid('on')\n",
    "ax.set_xlabel('Space')\n",
    "ax.set_ylabel('Height')\n",
    "line, = ax.plot([], [], lw=2)\n",
    "\n",
    "# initialization function: plot the background of each frame\n",
    "def init():\n",
    "    line.set_data([], [])\n",
    "    #line.grid('on')\n",
    "    return line,\n",
    "\n",
    "# animation function.  This is called sequentially\n",
    "def animate(i):\n",
    "    line.set_data(xplot, hsoln[i,:])\n",
    "    return line,\n",
    "\n",
    "# call the animator.  blit=True means only re-draw the parts that have changed.\n",
    "anim = animation.FuncAnimation(fig, animate, init_func=init,\n",
    "                               frames=Nt, interval=20, blit=True)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "HTML(anim.to_html5_video())"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "anim.save('simple_wave.mp4', fps=30, extra_args=['-vcodec', 'libx264'])"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 - Firedrake",
   "language": "python",
   "name": "opt-firedrake"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.5.2"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
