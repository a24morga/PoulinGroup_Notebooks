{
 "cells": [
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Expand the view to scale with window width\n",
    "from IPython.core.display import display, HTML\n",
    "display(HTML(\"<style>.container { width:90% !important; }</style>\"))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<h1 align=\"center\">\n",
    "ELMS1\n",
    "</h1>\n",
    "<h2 align=\"center\">\n",
    "One Dimensional Diffusions Equation\n",
    "</h2>\n",
    "<h3 align=\"center\">\n",
    "By Francis J. Poulin, Adam Morgan, and Ben Storer\n",
    "</h3>\n",
    "<h4 align=\"center\">\n",
    "Last modified: 31 January, 2018\n",
    "</h4>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Model Equations ##\n",
    "The one-dimensional (1D) diffusion equation with variable Diffusivity $K(x)$ is\n",
    "$$\n",
    "\\frac{\\partial u}{\\partial t} = \\frac{\\partial}{\\partial {x}} \\left(K(x)\\frac{\\partial u}{\\partial x}\\right).\n",
    "$$\n",
    "This is a **Parabolic PDE** of the parabolic type with \n",
    "\\begin{align*}\n",
    "\\rho(x) &= 1,\\\\\n",
    "p(x) &= K(x), \\\\ \n",
    "q(x) &= 0.\n",
    "\\end{align*}\n",
    "\n",
    "We have the option to impose two sets of boundary conditions and one initial condition.\n",
    "\n",
    "### Dirichlet: ###\n",
    "$$\n",
    "u(0,t) = 0,\n",
    "\\quad \\mbox{ and } \\quad\n",
    "u(Lx,t) = 0.\n",
    "$$\n",
    "\n",
    "### Neumann: ###\n",
    "$$\n",
    "\\frac{\\partial u}{\\partial x}(0,t) = 0,\n",
    "\\quad \\mbox{ and } \\quad\n",
    "\\frac{\\partial u}{\\partial x}(Lx,t) = 0.\n",
    "$$\n",
    "\n",
    "### Initial Condition ###\n",
    "$$\n",
    "u(x,0) = f(x).\n",
    "$$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Weak Form ##\n",
    "\n",
    "In this tutorial we look at numerical solutions to the non-uniform diffusion equation, as written above.   Instead of discretizing this PDE and solving it directly, we instead form what is called a weak form.  This allows us to use a **Finite Element Method** (FE).  This method is perhaps more complicated than say the **Finite Difference** (FD) Method, but one of the advantages is that it allows us to use more complex geometries.  This is not something we see in 1D but we will better appreciate in 2D.\n",
    "\n",
    "Since the PDE involves both spatial and temporal derivatives, we must find a way to approximate each. Our approach is to use a FD method in time (2nd order Crank-Nickolson) and a FE method in space (1st order).\n",
    "\n",
    "#### Temporal Discretization ####\n",
    "\n",
    "If we define $u_n(x) = u(x, t_n)$ then our time-stepping scheme is\n",
    "$$\n",
    "u^{n+1}  = u^n + \\Delta t\n",
    "\\left[ \\frac{\\partial}{\\partial {x}} \\left(K(x)\\frac{\\partial}{\\partial x}\\left( \\frac{u^{n+1} + u^n}{2} \\right) \\right)\\right]\n",
    "$$\n",
    "\n",
    "#### Spatial Approximation ####\n",
    "\n",
    "Next, we obtain the weak form of this equation by multiplying it by a test function, say $v$, to obtain the new solution, $u^*$, in terms of the known solution, say $u^n$.\n",
    "\\begin{align*}\n",
    "\\int v u^* \\, dx  \n",
    "& = \\int v u^n \\, dx + \\Delta t\n",
    "\\int v \\left[ \\frac{\\partial}{\\partial {x}} \\left(K(x)\\frac{\\partial}{\\partial x}\\left( \\frac{u^* + u^n}{2} \\right) \\right)\\right] \\, dx, \\\\\n",
    "\\int v u^* \\, dx & = \\int v u^n \\, dx +  \\Delta t \\left[ K(x) v \\frac{\\partial}{\\partial x}\\left( \\frac{u^* + u^n}{2} \\right) \\right]_0^{L_x}\n",
    "- \\Delta t \\int \\frac{\\partial v}{\\partial {x}} \\left(K(x)\\frac{\\partial}{\\partial x}\\left( \\frac{u^* + u^n}{2} \\right) \\right) \\, dx, \\\\\n",
    "\\int v u^*  + \\frac{\\Delta t}{2}  K(x) \\frac{\\partial v}{\\partial {x}} \\frac{\\partial u^*}{\\partial x} \\, dx & =  \\int v u^n  \n",
    "- \\frac{\\Delta t}{2}  K(x) \\frac{\\partial v}{\\partial {x}}\\frac{\\partial u^n}{\\partial x} \\, dx, \\\\\n",
    "L(v) & = a(u,v).\n",
    "\\end{align*}\n",
    "\n",
    "The right hand side is a bilinear form, which we denote with $a(u,v)$.\n",
    "\n",
    "The left hand side is a linear form, which we denote with $L(v)$.\n",
    "\n",
    "Note that in the above we have that the boundary terms vanish if either we impose zero Dirichlet or Neumann boundary conditions.  This assumption can be weakened but this is sufficiently interesting for us that we only consider these two conditions.\n",
    "\n",
    "At each time step we know the given solution, $u^n$, and we solve the above linear system for the new solution, $u^*$, and that is our $u^{n+1}$."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Numerical Solution#\n",
    "\n",
    "Below we go through each step of the numerical method to solve the above weak form.\n",
    "\n",
    "#### Import Libraries"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "#from firedrake import *\n",
    "import firedrake as fdr\n",
    "import numpy as np\n",
    "import matplotlib.pyplot as plt\n",
    "import matplotlib.animation as animation\n",
    "import cmocean\n",
    "from IPython.display import HTML\n",
    "import sys"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Specify parameters"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "T  = 2.0          # Final time\n",
    "dt = 0.02         # Time step \n",
    "Nt = np.int(T/dt) # Number of time steps\n",
    "Dt = fdr.Constant(dt)\n",
    "\n",
    "Bdry = 'Dirichlet'\n",
    "#Bdry = 'Neumann'\n",
    "\n",
    "Lx = 6.0          # Length of domain\n",
    "Nx = 600          # Number of elements\n",
    "\n",
    "deg  = 1          # Order of approximation"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Create the grid"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Time\n",
    "tt = np.linspace(0., T, Nt)\n",
    "\n",
    "# Space\n",
    "mesh  = fdr.IntervalMesh(Nx, Lx)\n",
    "x     = fdr.SpatialCoordinate(mesh)\n",
    "xplot = np.linspace(0, Lx, Nx)\n",
    "\n",
    "# Define Function Space\n",
    "V = fdr.FunctionSpace(mesh, \"CG\", deg)         # Corresponds to  P_{deg}\\Lambda^{0}(mathcal{T})"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Set the boundary conditions"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "if Bdry == 'Dirichlet':\n",
    "    \n",
    "    #Remark: The DirichletBC command needs three arguments: \n",
    "    # function space, the value and which boundary endpoint \n",
    "    # Note: (\"1\" for left and \"2\" for right)    \n",
    "    \n",
    "    bcLeft  = fdr.DirichletBC(V, fdr.Constant(0.0), 1)\n",
    "    bcRight = fdr.DirichletBC(V, fdr.Constant(0.0), 2)\n",
    "\n",
    "    bcs = [bcLeft, bcRight]\n",
    "    #bcs = [bcLeft]\n",
    "    \n",
    "elif Bdry == 'Neumann':\n",
    "\n",
    "    bcs = []\n",
    "    \n",
    "else:\n",
    "\n",
    "    print('Must select Dirichlet or Neumann boundary conditions')\n",
    "    sys.exit()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Provide the diffusivity function $K(x)$\n",
    "\n",
    "Opt1: $ 1 + \\exp\\left( -5\\left(x-\\frac{Lx}{2}\\right)^2 \\right) $\n",
    "\n",
    "Opt2: $ 1 + 2\\left(\\frac{x}{Lx} - 0.5\\right)$\n",
    "\n",
    "Opt3: $1 + \\tanh\\left[10 \\cdot \\left(\\frac{x}{Lx} - \\frac{1}{2}\\right)\\right] $\n",
    "\n",
    "Opt4: $1 + 0.5\\sin\\left[ 2 \\pi \\left( x - \\frac{Lx}{2}\\right) / \\frac{Lx}{2} \\right]$"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "K = fdr.Function(V)\n",
    "\n",
    "#K.interpolate(Expression(\"1.0 + exp(-5*pow(x[0]-Lx/2.,2))\", Lx=Lx));\n",
    "\n",
    "#K.interpolate(Expression(\"1.0 + (x[0]-Lx/2)/Lx*2\", Lx=Lx));\n",
    "\n",
    "#K.interpolate(Expression(\" 1 + tanh((x[0] - Lx/2)/(Lx/10))\", Lx=Lx));\n",
    "\n",
    "K.interpolate(fdr.Expression(\" 1 + 0.9*sin(2*pi*(x[0] - Lx/2)/(Lx/2))\", Lx=Lx));"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Plot $K$ \n",
    "\n",
    "Note that here we are using the firedrake-specific plotting features,\n",
    "since we are plotting a Firedrake object. Under the hood it is still using\n",
    "matplotlib, but it is tucked away."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "plt.figure()\n",
    "fdr.plot(K)\n",
    "plt.title('Diffusion coefficient')\n",
    "plt.xlim([0, Lx])\n",
    "plt.xlabel('space')\n",
    "plt.grid('on')\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Specify initial conditions"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Define solutions\n",
    "u0 = fdr.Function(V)    \n",
    "u1 = fdr.Function(V)    \n",
    "\n",
    "# Pick initial conditions\n",
    "u0.interpolate(fdr.Expression(\"exp(-9*pow(x[0]-Lx/2.,2))\", Lx=Lx));\n",
    "\n",
    "# Constant: interesting with Dirichlet BCs\n",
    "#u0.interpolate(fdr.Expression(\"1.0\"));"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Plot the inital conditions"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "plt.figure()\n",
    "fdr.plot(u0)\n",
    "plt.title('Initial Conditions')\n",
    "plt.xlim([0, Lx])\n",
    "plt.xlabel('space')\n",
    "plt.grid('on')\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def Step_Diffusion(u0, K, bcs):\n",
    "    \n",
    "    # Define functions to build weak form\n",
    "    v = fdr.TestFunction(V)\n",
    "    u = fdr.TrialFunction(V)\n",
    "    \n",
    "    # Define weak form\n",
    "    # \"a\" is a bilinear form: a(u,v)\n",
    "    # \"L\" is a linear form:   L(v)\n",
    "    a = (v*u  + 0.5*K*Dt*( u.dx(0)*v.dx(0)))*fdr.dx\n",
    "    L = (v*u0 - 0.5*K*Dt*(u0.dx(0)*v.dx(0)))*fdr.dx  \n",
    "\n",
    "    # Solve weak form\n",
    "    fdr.solve(a == L, u1, solver_parameters={'mat_type': 'aij','ksp_type': 'preonly','pc_type': 'lu'}, bcs = bcs)\n",
    "\n",
    "    return u1"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Create arrays to store the solution"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "usoln      = np.zeros((Nt,Nx))\n",
    "usoln[0,:] = u0.at(xplot, tolerance=1e-12)\n",
    "\n",
    "mass = np.zeros(Nt)\n",
    "mass[0] = fdr.assemble(u0*fdr.dx)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Loop over the time-steps, storing the solution as we go"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "for cnt in range(1,Nt):\n",
    "    \n",
    "    t = cnt*dt\n",
    "    \n",
    "    u1 = Step_Diffusion(u0, K, bcs)\n",
    "    \n",
    "    u0.assign(u1)\n",
    "    mass[cnt] = fdr.assemble(u0*fdr.dx)\n",
    "    \n",
    "    usoln[cnt,:] = u0.at(xplot, tolerance=1e-12)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Plot mass and relative change\n",
    "\n",
    "Note that in the case of Dirichlet boundary conditions, we do not expect mass/energy conservation. To see this, consider the long-term behaviour of the system."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Compute relative change\n",
    "relerror = (mass[0] - mass)/mass[0]\n",
    "\n",
    "# Create figure and two y-axes\n",
    "plt.figure()\n",
    "ax1 = plt.gca()\n",
    "ax2 = ax1.twinx()\n",
    "\n",
    "# Plot the mass and relative change on the two axes\n",
    "ax1.plot(tt, mass,   'b', lw=2)\n",
    "ax2.plot(tt, relerror, 'r', lw=2)\n",
    "\n",
    "# Specify axis labels\n",
    "ax1.set_xlabel('time')\n",
    "ax1.set_ylabel('Mass (blue)')\n",
    "ax2.set_ylabel('Relative change (red)')\n",
    "\n",
    "# Add a grid\n",
    "plt.grid('on')\n",
    "\n",
    "# Fix some layout stuff\n",
    "plt.tight_layout(True)\n",
    "\n",
    "# Not strictly necessary in iPython\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Plot the evolution of the system"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "plt.figure()\n",
    "plt.pcolor(xplot, tt, usoln, cmap = cmocean.cm.thermal)\n",
    "plt.xlabel(\"x\")\n",
    "plt.ylabel(\"t\")\n",
    "plt.title(\"Contour Plot of u\")\n",
    "plt.colorbar()\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Create animations"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "fig = plt.figure()\n",
    "ax = plt.axes(xlim=(0, 6.0), ylim=(0.0, 1.1))\n",
    "ax.grid('on')\n",
    "ax.set_xlabel('Space')\n",
    "ax.set_ylabel('Height')\n",
    "line, = ax.plot([], [], lw=2)\n",
    "\n",
    "# initialization function: plot the background of each frame\n",
    "def init():\n",
    "    line.set_data([], [])\n",
    "    #line.grid('on')\n",
    "    return line,\n",
    "\n",
    "# animation function.  This is called sequentially\n",
    "def animate(i):\n",
    "    line.set_data(xplot, usoln[i,:])\n",
    "    return line,\n",
    "\n",
    "# call the animator.  blit=True means only re-draw the parts that have changed.\n",
    "anim = animation.FuncAnimation(fig, animate, init_func=init,\n",
    "                               frames=Nt, interval=20, blit=True)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "HTML(anim.to_html5_video())"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "anim.save('simple_diffusion_VarConductivity.mp4', fps=30, extra_args=['-vcodec', 'libx264'])"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 - Firedrake",
   "language": "python",
   "name": "opt-firedrake"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.5.2"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
